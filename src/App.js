import './App.css';
import Header from './components/Header';
import Profile from './components/Profile';
import About from './components/About';
import ProjectSection from './components/Project';
import Skills from './components/Skills';
import Resume from './components/Resume';
import Footer from './components/Footer';
import { Box } from '@chakra-ui/react';

function App() {
  return (
    <>
      <Header/>
      <Profile/>
      {/* <Box className='container'> */}
      <Resume/>
      <About />
      <ProjectSection/>
      <Skills/>
      <hr style={{borderTopWidth:'1px', borderColor:'rgba(255, 255, 255, 0.16)'}}/>
      <Footer/>
      {/* </Box> */}
    </> 
  );
}

export default App;
