import React from "react";
import { Box, Heading, Text, Grid,} from "@chakra-ui/react";
import CircularPhoto from "./CircularPhoto";

const Profile = () => {
  return (
    <>
      <Grid templateColumns={{ base: '1fr', md: '2fr 1fr' }}  gap={{base:'2rem',md:'0rem'}} className="container" pt={{base:"2rem",md:'5rem'}} id="profile" width={{base:'98%',md:'70%'}}>
        <Box order={[2,1]}>
          <Heading fontWeight="700" 
          fontSize={{base:'3rem',md:"6rem"}} textAlign={{base:'center',md:'start'}} letterSpacing='-0.0125em' className="hfont">
            Vivek Mishra
          </Heading>
          <Text className="hfont" textAlign={{base:'center',md:'start'}} fontSize={{base:'3xl', md:'4xl'}} mb={8} letterSpacing='-0.0225em' color="hsl(28, 50.0%, 63.1%)" >
            FullStack Web Developer
          </Text>
          <Text fontSize="xl" maxWidth="32rem" color="#d6d3d1" letterSpacing='-0.0125em' textAlign={{base:'center',md:'start'}}>
          Passionate Fullstack Developer eager to contribute to dynamic projects, combining creativity and technical expertise to deliver exceptional user experiences
          </Text>
        </Box>
        <CircularPhoto />
      </Grid>
    </>
  );
};

export default Profile;
