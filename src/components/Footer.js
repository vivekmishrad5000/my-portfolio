import React from 'react'
import { Box,Text,Heading, Flex, Link } from '@chakra-ui/react'
import GitHubIcon from '@mui/icons-material/GitHub';
import EmailIcon from '@mui/icons-material/Email';
import { RxLinkedinLogo } from "react-icons/rx";



const Footer = () => {
  return (
    <>
      <Box mt='5rem' pb='5rem' color="#d6d3d1" className='container'>
        <Heading as='h2' className='hfont' fontSize='1.875rem' color='#d6d3d1'>Vivek Mishra</Heading>
        <Text mt='1.5rem' fontSize='1.125rem'>Bridging the gap between vision and reality through fullstack mastery.</Text>
        <Flex mt='2rem'>
        <Link
              className="navElements"
              href="https://www.linkedin.com/in/vivek-mishra-8b6774306/"
              target='_blank'
              color="#d6d3d1"
              mr='2rem'
              _hover={{
                color: "hsl(28, 50.0%, 63.1%)",
              }}
            >
              <RxLinkedinLogo
                fontSize="large"
                style={{
                  color: "hsl(28, 50.0%, 63.1%)",
                  marginRight: "0.5rem",
                }}
              />
              <Box borderBottom='1px solid white' fontSize='1rem'>LinkedIn</Box>
            </Link>

        <Link
              className="navElements"
              href="https://gitlab.com/vivekmishrad5000"
              target='_blank'
              color="#d6d3d1"
              mr='2rem'
              _hover={{
                color: "hsl(28, 50.0%, 63.1%)",
              }}
            >
              <GitHubIcon
                fontSize="small"
                style={{
                  color: "hsl(28, 50.0%, 63.1%)",
                  marginRight: "0.5rem",
                }}
              />
              <Box borderBottom='1px solid white' fontSize='1rem'>Github</Box>
            </Link>

        <Link
              className="navElements"
              href="mailto:vivekmishrad5000@gmail.com"
              target='_blank'
              color="#d6d3d1"
              _hover={{
                color: "hsl(28, 50.0%, 63.1%)",
              }}
            >
              <EmailIcon
                fontSize="small"
                style={{
                  color: "hsl(28, 50.0%, 63.1%)",
                  marginRight: "0.5rem",
                }}
              />
              <Box borderBottom='1px solid white' fontSize='1rem'>Email</Box>
            </Link>
        </Flex>
        <Text mt='5rem' opacity='0.7'>All rights reserved © Vivek Mishra 2024</Text>
      </Box>
    </>
  )
}

export default Footer