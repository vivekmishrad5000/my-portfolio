import React from "react";
import {Link,Heading, Flex, Box } from "@chakra-ui/react";

const Skills = () => {
  return (
    <>
      <Heading as="h2" className="hfont container" fontSize={{base:'2.8rem', md:"3.75rem"}} color="#d6d3d1" id='skills' letterSpacing='-0.0225em' textAlign={{base:'center', md:'start'}}>
        Tools & Softwares Skills
      </Heading>
      <Box className="container">
      <Flex pb='6.25rem'  gap='2rem' pt='3rem'  width='90%' flexWrap='wrap' letterSpacing='-0.0125em' fontSize='1.875rem' color='hsl(28, 50.0%, 63.1%)' _hover={{textDecoration:'none'}} ml={{base:'3rem', md:'0'}}>
        <Link _hover={{textDecoration:'none'}}>HTML5</Link>
        <Link _hover={{textDecoration:'none'}}>CSS3</Link>
        <Link _hover={{textDecoration:'none'}}>JavaScript</Link>
        <Link _hover={{textDecoration:'none'}}>React</Link>
        <Link _hover={{textDecoration:'none'}}>Node.Js</Link>
        <Link _hover={{textDecoration:'none'}}>Express</Link>
        <Link _hover={{textDecoration:'none'}}>MongoDB</Link>
        <Link _hover={{textDecoration:'none'}}>Postman</Link>
        <Link _hover={{textDecoration:'none'}}>Rest API</Link>
        <Link _hover={{textDecoration:'none'}}>Git</Link>
        <Link _hover={{textDecoration:'none'}}>ChakraUI</Link>
        <Link _hover={{textDecoration:'none'}}>Tailwind</Link>
        <Link _hover={{textDecoration:'none'}}>VS Code</Link>
      </Flex>
      </Box>
      <hr style={{borderTopWidth:'1px', borderColor:'rgba(255, 255, 255, 0.16)'}}/>
    </>
  );
};

export default Skills;
