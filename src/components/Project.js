import React from "react";
import { Flex, Box, Text, Grid, Heading, Link, Image } from "@chakra-ui/react";
import { FaGitlab } from "react-icons/fa6";
import LanguageIcon from "@mui/icons-material/Language";
import image from "../components/images/rststore.png";
import newsImage from "../components/images/top-headlines.png";

const Project = () => {
  return (
    <>
      <Box as='section' id='projects' className="container" pb='5rem'>
        <Heading as="h2" textAlign={{base:'center', md:'start'}} className="hfont" fontSize={{base:'2.9rem', md:"3.75rem"}} letterSpacing="-0.025em" color="#d6d3d1">
          Featured Projects
        </Heading>
        <Grid templateColumns="1fr 1fr" pb="3rem" display={{base:'flex', md:'grid'}} flexDirection={{base:'column'}}>
          <Box mt="4rem">
            <Heading as="h4" className='hfont' color="hsl(28, 50.0%, 63.1%)" letterSpacing='-0.0225em' textAlign={{base:'center', md:'start'}}>
              Fullstack Ecomm site
            </Heading>
            <Flex mt="1.6rem">
              <Link
              target="_blank"
                href="https://gitlab.com/vivekmishrad5000/modern-store"
                display="flex"
                ml={{base:'4rem', md:'0'}}
                color="#d6d3d1"
                _hover={{
                  textDecoration: "none",
                  color: "hsl(28, 50.0%, 63.1%)",
                }}
              >
              <Box display='flex' alignItems='center'>
                <FaGitlab/>
                </Box>
                <Box ml="0.5rem" borderBottom="1px solid white">
                  GitLab
                </Box>
              </Link>

              <Link
                href="http://172.104.206.152"
                target='_blank'
                display="flex"
                ml="2rem"
                color="#d6d3d1"
                _hover={{
                  textDecoration: "none",
                  color: "hsl(28, 50.0%, 63.1%)",
                }}
              >
                <LanguageIcon />
                <Box ml="0.5rem" borderBottom="1px solid white">
                  Website
                </Box>
              </Link>
            </Flex>
            <Text mt="1.5rem" width={{base:'98%', md:"25rem"}} color="#d6d3d1" fontSize='xl' letterSpacing='-0.0225em' textAlign={{base:'center', md:'start'}}>
              Crafted an innovative clothing ecommerce platform, harnessing React for dynamic UIs, Express for robust server-side operations, and MongoDB for scalable data storage. Seamlessly integrated with PayPal for secure transactions, enhanced by Redux for efficient state management, delivering a smooth shopping experience with secure payments and intuitive navigation and also incorporates admin features for managing products, orders, and users.
            </Text>
          </Box>
          <Box
            mt={{base:'3rem', md:'5rem'}}
            height="25rem"
            position="relative"
            overflow="hidden"   
            borderRadius="1rem"
            background="linear-gradient(180deg, #FEB48C 0%, #1EBBFF 100%)"
          >
            <Box
              position="absolute"
              left="2.5rem"
              top="2.5rem"
              overflow="hidden"
              width="56.25rem"
              height="31.25rem"
              borderRadius="0.5rem"
            >
              <Image
                src={image}
                alt="project"
                height="400px"
                width="490px"
               
              />
            </Box>
          </Box>
        </Grid>


        <Grid templateColumns="1fr 1fr" pb="3rem" display={{base:'flex', md:'grid'}} flexDirection={{base:'column'}}>
          <Box mt="4rem">
            <Heading as="h4" className='hfont' color="hsl(28, 50.0%, 63.1%)" letterSpacing='-0.0225em' textAlign={{base:'center', md:'start'}}>
              News Aggregator
            </Heading>
            <Flex mt="1.6rem">
              <Link
              target="_blank"
                href="https://gitlab.com/vivekmishrad5000/news-aggregator"
                display="flex"
                ml={{base:'4rem', md:'0'}}
                color="#d6d3d1"
                _hover={{
                  textDecoration: "none",
                  color: "hsl(28, 50.0%, 63.1%)",
                }}
              >
              <Box display='flex' alignItems='center'>
                <FaGitlab/>
                </Box>
                <Box ml="0.5rem" borderBottom="1px solid white">
                  GitLab
                </Box>
              </Link>

            
            </Flex>
            <Text mt="1.5rem" width={{base:'98%', md:"25rem"}} color="#d6d3d1" fontSize='xl' letterSpacing='-0.0225em' textAlign={{base:'center', md:'start'}}>
            Designed and developed a dynamic news aggregation platform using React, enriched with essential features including categorization and search functionalities. Leveraging React's versatility, users can seamlessly explore news across various categories, ensuring a tailored experience. The addition of a robust search feature empowers users to swiftly discover relevant content. Engineered with an eye for user experience, this project merges functionality with elegance, delivering a comprehensive news browsing solution.
            </Text>
          </Box>
          <Box
            mt={{base:'3rem', md:'6rem'}}
            height="25rem"
            position="relative"
            overflow="hidden"   
            borderRadius="1rem"
            background="linear-gradient(180deg, #FEB48C 0%, #1EBBFF 100%)"
          >
            <Box
              position="absolute"
              left="2.5rem"
              top="2.5rem"
              overflow="hidden"
              width="56.25rem"
              height="31.25rem"
              borderRadius="0.5rem"
            >
              <Image
                src={newsImage}
                alt="project"
                height="400px"
                width="490px"
               
              />
            </Box>
          </Box>
        </Grid>
      </Box>
      <Box>

       
      </Box>
    </>
  );
};

export default Project;
