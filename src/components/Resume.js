import React from 'react'
import { Button, Box, Link } from '@chakra-ui/react'

const Resume = () => {
  return (
    <>
    <Box  id='resume' className='container'>
    <Box pt='3.5rem' mx={{base:'3.5rem', md:'0'}} pb='1rem' position='relative'>
       <Link href='VIVEK_RESUME_8451081905.pdf' download='VIVEK_RESUME_8451081905.pdf'> <Button background='#EDF2F7' size='lg'  _hover={{color:'hsl(28deg 100% 17.95%)', background:'#d6d3d1'}} >Checkout My Resume</Button>
       </Link>
    </Box>
    </Box>
    
    </>
  )
}

export default Resume