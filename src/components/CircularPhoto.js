import { Box, Image } from '@chakra-ui/react';
import React from 'react';
import image from "./images/vivek.jpg";


const CircularPhoto = ({ src, alt }) => {
  return (
    <Box
      borderRadius="full"
      overflow="hidden"
      width={{base:'200px' ,md:'300px'}}  // Adjust width and height as needed
      height={{base:'200px' ,md:'300px'}} // Adjust width and height as needed
      boxShadow="md"
      bg="white"
      order={[1,2]}
      ml={{base:'6rem',md:'0'}}
    >
      <Image src={image} alt={'divyansh'} objectFit="cover" width="100%" height="100%" />
    </Box>
  );
};

export default CircularPhoto;