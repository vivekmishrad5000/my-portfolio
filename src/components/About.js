import React from "react";
import { Grid, Box, Link, Heading, Text,} from "@chakra-ui/react";
import { RxLinkedinLogo } from "react-icons/rx";
import TextSnippetIcon from "@mui/icons-material/TextSnippet";
import { FaGitlab } from "react-icons/fa6";
import EmailIcon from "@mui/icons-material/Email";

const About = () => {
  return (
    <>
      <Grid templateColumns="1fr 1fr" mt={{base:'6rem', md:"5rem"}} mb={{base:'8rem', md:"10rem"}} pb="2rem"  className="container" display={{base:'flex',md:'grid'}} flexDirection={{base:'column',md:'column'}}>
        <Box>
          <Heading fontSize={{base:'4rem', md:"8xl"}} color="#d6d3d1" className="hfont" letterSpacing='-0.0225em' textAlign={{base:'center',md:'start'}}>
            I Design{" "}
            <span style={{ color: "hsl(28, 50.0%, 63.1%)" }}>
              Monolith Application
            </span>
          </Heading>
        </Box>
        <Box ml={{base:'0',md:"6rem"}} maxWidth="30rem" mt="2rem">
          <Text fontSize="xl" color="#d6d3d1" letterSpacing='-0.0225em' textAlign={{base:'center',md:'start'}}>
          Greetings! I'm Vivek Mishra, a Fullstack Developer passionate about crafting seamless digital experiences. With expertise in front-end design and back-end development, I specialize in turning innovative ideas into functional solutions. Explore my portfolio to see how I bring projects to life through code.
          </Text>
          <Grid templateColumns="1fr 4fr" gridGap="2.5rem" mt="2.5rem" ml={{base:'3rem',md:'0'}}>
            <Link
              className="navElements"
              target="_blank"
              href="https://www.linkedin.com/in/vivek-mishra-8b6774306/"
              color="#d6d3d1"
              _hover={{
                color: "hsl(28, 50.0%, 63.1%)",
              }}
            >
              <RxLinkedinLogo
                fontSize="large"
                style={{
                  color: "hsl(28, 50.0%, 63.1%)",
                  marginRight: "0.5rem",
                }}
              />
              <Box borderBottom="1px solid white" fontSize="1rem">
                LinkedIn
              </Box>
            </Link>
            <Link
              className="navElements"
              target='_blank'
              href="https://gitlab.com/vivekmishrad5000"
              color="#d6d3d1"
              _hover={{
                textDecoration: "none",
                color: "hsl(28, 50.0%, 63.1%)",
              }}
            >
              <FaGitlab
                fontSize='large'
                style={{
                  color: "hsl(28, 50.0%, 63.1%)",
                  marginRight: "0.5rem",
                }}
              />
              <Box borderBottom="1px solid white" fontSize="1rem" textAlign={{base:'center',md:'start'}}>
                GitLab
              </Box>
            </Link>
            <Link
              className="navElements"
              href='mailto:vivekmishrad5000@gmail.com'
              color="#d6d3d1"
              _hover={{
                textDecoration: "none",
                color: "hsl(28, 50.0%, 63.1%)",
              }}
            >
              <EmailIcon
                fontSize="small"
                style={{
                  color: "hsl(28, 50.0%, 63.1%)",
                  marginRight: "0.5rem",
                }}
              />
              <Box borderBottom="1px solid white" fontSize="1rem">
                Email
              </Box>
            </Link>
            <Link
              className="navElements"
              href="#"
              color="#d6d3d1"
              _hover={{
                textDecoration: "none",
                color: "hsl(28, 50.0%, 63.1%)",
              }}
            >
              <TextSnippetIcon
                fontSize="small"
                style={{
                  color: "hsl(28, 50.0%, 63.1%)",
                  marginRight: "0.5rem",
                }}
              />
              <Box borderBottom="1px solid white" fontSize="1rem">
                Resume
              </Box>
            </Link>
          </Grid>
        </Box>
      </Grid>
    </>
  );
};

export default About;
