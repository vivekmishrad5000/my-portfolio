import React from "react";
import {
  Box,
  Flex,
  Avatar,
  Link,
  Icon,
  Drawer,
  DrawerBody,
  DrawerOverlay,
  DrawerContent,
} from "@chakra-ui/react";
import { Link as ScrollLink } from "react-scroll";
import HomeIcon from "@mui/icons-material/Home";
import PersonIcon from "@mui/icons-material/Person";
import TaskIcon from "@mui/icons-material/Task";
import TipsAndUpdatesIcon from "@mui/icons-material/TipsAndUpdates";
import TextSnippet from "@mui/icons-material/TextSnippet";
import { HiOutlineMenuAlt3 } from "react-icons/hi";
import { useState } from "react";
import { useDisclosure } from "@chakra-ui/react";

const Header = () => {
  const [open, setOpen] = useState(false);
  const { isOpen, onOpen, onClose } = useDisclosure();
  const [placement] = React.useState("bottom");

  return (
    <Flex
      as="nav"
      align="center"
      justify="space-between"
      wrap="wrap"
      padding="1.5rem"
      color="white"
      className="container"
    >
      <Flex align="center" mr={5}>
        <Box as="h1" fontSize="xl" fontWeight="bold">
          <Avatar name="Vivek Mishra" backgroundColor='hsl(28, 50.0%, 63.1%)' />
        </Box>
      </Flex>
      <Flex>
        <Box
          display={{ base: "block", md: "none" }}
          onClick={() => setOpen(!open)}
        >
          <Icon
            as={HiOutlineMenuAlt3}
            color="white"
            w="10"
            h="10"
            onClick={onOpen}
          />
        </Box>

        {/* NAVELEMENTS */}
        <Box
          display={{ base: open ? "block" : "none", md: "flex" }}
          width={{ base: "full", md: "auto" }}
          mt={{ base: "3", md: "0" }}
        >
          <Drawer placement={placement} onClose={onClose} isOpen={isOpen}>
            <DrawerOverlay />
            <DrawerContent backgroundColor="#1c1917">

              <DrawerBody>
                <Link
                  className="navElements"
                  href="/"
                  color="#d6d3d1"
                  p='1.4rem 0 1.4rem 0'
                  _hover={{
                    textDecoration: "none",
                    color: "hsl(28, 50.0%, 63.1%)",
                  }}
                >
                  <HomeIcon
                    fontSize="small"
                    style={{
                      color: "hsl(28, 50.0%, 63.1%)",
                      marginRight: "0.5rem",
                    }}
                  />
                  <Box>Home</Box>
                </Link>
                <hr style={{borderTopWidth:'1px', borderColor:'rgba(255, 255, 255, 0.16)'}}/>
                <ScrollLink
                  to="profile"
                  smooth={true}
                  duration={500}
                  onClick={onClose}
                >
                  <Link
                    className="navElements"
                    href="/profile"
                    color="#d6d3d1"
                    p='1.4rem 0 1.4rem 0'
                    _hover={{
                      textDecoration: "none",
                      color: "hsl(28, 50.0%, 63.1%)",
                    }}
                  >
                    <PersonIcon
                      fontSize="small"
                      style={{
                        color: "hsl(28, 50.0%, 63.1%)",
                        marginRight: "0.5rem",
                      }}
                    />
                    <Box>About</Box>
                  </Link>
                  <hr style={{borderTopWidth:'1px', borderColor:'rgba(255, 255, 255, 0.16)'}}/>
                </ScrollLink>

                <ScrollLink
                  to="projects"
                  smooth={true}
                  duration={500}
                  onClick={onClose}
                >
                  <Link
                    className="navElements"
                    href="#"
                    color="#d6d3d1"
                    p='1.4rem 0 1.4rem 0'
                    _hover={{
                      textDecoration: "none",
                      color: "hsl(28, 50.0%, 63.1%)",
                    }}
                  >
                    <TaskIcon
                      fontSize="small"
                      style={{
                        color: "hsl(28, 50.0%, 63.1%)",
                        marginRight: "0.5rem",
                      }}
                    />
                    <Box>Projects</Box>
                  </Link>
                  <hr style={{borderTopWidth:'1px', borderColor:'rgba(255, 255, 255, 0.16)'}}/>
                </ScrollLink>

                <ScrollLink
                  to="skills"
                  smooth={true}
                  duration={500}
                  onClick={onClose}
                >
                  <Link
                    className="navElements"
                    href="#"
                    color="#d6d3d1"
                    p='1.4rem 0 1.4rem 0'
                    _hover={{
                      textDecoration: "none",
                      color: "hsl(28, 50.0%, 63.1%)",
                    }}
                  >
                    <TipsAndUpdatesIcon
                      fontSize="small"
                      style={{
                        color: "hsl(28, 50.0%, 63.1%)",
                        marginRight: "0.5rem",
                      }}
                    />
                    <Box>Skills</Box>
                  </Link>
                  <hr style={{borderTopWidth:'1px', borderColor:'rgba(255, 255, 255, 0.16)'}}/>
                </ScrollLink>

                <ScrollLink
                  to="resume"
                  smooth={true}
                  duration={500}
                  onClick={onClose}
                >
                  <Link
                    className="navElements"
                    href="#"
                    color="#d6d3d1"
                    p='1.4rem 0 1.4rem 0'
                    _hover={{
                      textDecoration: "none",
                      color: "hsl(28, 50.0%, 63.1%)",
                    }}
                  >
                    <TextSnippet
                      fontSize="small"
                      style={{
                        color: "hsl(28, 50.0%, 63.1%)",
                        marginRight: "0.5rem",
                      }}
                    />
                    <Box>Resume</Box>
                  </Link>
                </ScrollLink>
              </DrawerBody>
            </DrawerContent>
          </Drawer>
          <Box display={{ base: "none", md: "flex" }}>
            <Link
              className="navElements"
              href="/"
              color="#d6d3d1"
              _hover={{
                textDecoration: "none",
                color: "hsl(28, 50.0%, 63.1%)",
              }}
            >
              <HomeIcon
                fontSize="small"
                style={{
                  color: "hsl(28, 50.0%, 63.1%)",
                  marginRight: "0.5rem",
                }}
              />
              <Box>Home</Box>
            </Link>

            <ScrollLink to="profile" smooth={true} duration={500}>
              <Link
                className="navElements"
                href="/profile"
                color="#d6d3d1"
                _hover={{
                  textDecoration: "none",
                  color: "hsl(28, 50.0%, 63.1%)",
                }}
              >
                <PersonIcon
                  fontSize="small"
                  style={{
                    color: "hsl(28, 50.0%, 63.1%)",
                    marginRight: "0.5rem",
                  }}
                />
                <Box>About</Box>
              </Link>
            </ScrollLink>

            <ScrollLink to="projects" smooth={true} duration={500}>
              <Link
                className="navElements"
                href="#"
                color="#d6d3d1"
                _hover={{
                  textDecoration: "none",
                  color: "hsl(28, 50.0%, 63.1%)",
                }}
              >
                <TaskIcon
                  fontSize="small"
                  style={{
                    color: "hsl(28, 50.0%, 63.1%)",
                    marginRight: "0.5rem",
                  }}
                />
                <Box>Projects</Box>
              </Link>
            </ScrollLink>

            <ScrollLink to="skills" smooth={true} duration={500}>
              <Link
                className="navElements"
                href="#"
                color="#d6d3d1"
                _hover={{
                  textDecoration: "none",
                  color: "hsl(28, 50.0%, 63.1%)",
                }}
              >
                <TipsAndUpdatesIcon
                  fontSize="small"
                  style={{
                    color: "hsl(28, 50.0%, 63.1%)",
                    marginRight: "0.5rem",
                  }}
                />
                <Box>Skills</Box>
              </Link>
            </ScrollLink>

            <ScrollLink to="resume" smooth={true} duration={500}>
              <Link
                className="navElements"
                href="#"
                color="#d6d3d1"
                _hover={{
                  textDecoration: "none",
                  color: "hsl(28, 50.0%, 63.1%)",
                }}
              >
                <TextSnippet
                  fontSize="small"
                  style={{
                    color: "hsl(28, 50.0%, 63.1%)",
                    marginRight: "0.5rem",
                  }}
                />
                <Box>Resume</Box>
              </Link>
            </ScrollLink>
          </Box>
        </Box>
      </Flex>
    </Flex>
  );
};

export default Header;
